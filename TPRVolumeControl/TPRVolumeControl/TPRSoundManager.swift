//
//  TPRSoundManager.swift
//  TPRVolumeControl
//
//  Created by SnowMan on 2016-09-03.
//  Copyright © 2016 test. All rights reserved.
//

import Foundation
import AVFoundation
import MediaPlayer

// const
let volumeSliderClassName = "MPVolumeSlider"

class TPRSoundManager {
	static let sharedInstance = TPRSoundManager()
	private let volumeControl:MPVolumeView
	private var defaultMediaPlayerVolume:Float	= -1.0
	
	//--------------------------------------------------------------------------------------------------------------
	// MARK: - Init
	//--------------------------------------------------------------------------------------------------------------

	init() {
		volumeControl = MPVolumeView()
	}
	
	convenience init(volume: NSNumber) {
		self.init()
		self.setMediaPlayerVolume(volume)
	}
	
	
	//--------------------------------------------------------------------------------------------------------------
	// MARK: - Init
	//--------------------------------------------------------------------------------------------------------------
	
	internal func setMediaPlayerVolume(volume: NSNumber){
		// Get slider to set volume
		volumeControl.subviews.forEach {
			if NSStringFromClass($0.classForCoder) == volumeSliderClassName {
				if let view = $0 as? UISlider {
					// Change volume level
					let volumeLevelNormalized = volume.floatValue/100
					view.setValue(volumeLevelNormalized, animated:false)
				}
			}
		}
	}
	
	internal func setMediaPlayerVolumeToDefault(){
		self.setMediaPlayerVolume(defaultMediaPlayerVolume)
	}
}