//
//  TPRVolumeControl.swift
//  TPRVolumeControl
//
//  Created by SnowMan on 2016-08-28.
//  Copyright © 2016 test. All rights reserved.
//

import Foundation
import UIKit


enum TPRVolumeControlInputField: Int {
	case volume = 0
	case lines
}

// const
private let inputFieldHeight:CGFloat	= 30.0
private let verticalSpacer:CGFloat		= 10.0
private let horiziontalSpacer:CGFloat	= 5.0
private let volumeLevelMAX:NSNumber		= 100.0
private let volumeLevelMIN:NSNumber		= 0.0
private let linesAmountMIN:NSNumber		= 0.0

@IBDesignable
class TPRVolumeControl: UIControl, UITextFieldDelegate {
	
	// Volume input
	private let volumeInputField:UITextField = UITextField()
	private let volumeInputButton:UIButton = UIButton()
	
	// Lines input
	private let linesInputField:UITextField = UITextField()
	private let linesInputButton:UIButton = UIButton()
	
	// Container for volume levels
	private var volumeLevelContainerView:UIView = UIView()
	private var volumeLevelContainerH = CGFloat()
	private var volumeLevelContainerW = CGFloat()
	
	// Volume level indicator label
	private let volumeLevelLabel = UILabel()
	
	// Helpers
	private let soundManager = TPRSoundManager.sharedInstance
	
	// Resize the volume lines container based on frame
	override var frame: CGRect {
		didSet {
			self.buildUI(frame)
		}
	}
	
	// Storyboard editable
	@IBInspectable
	var lineColor:UIColor = .blueColor(){
		didSet {
			redrawLines()
		}
	}
	
	@IBInspectable
	var volumeLevelPercentage:Int = 50 {
		didSet {
			self.changeVolumeLevel(volumeLevelPercentage)
			self.updateVolumeLevelLines(volumeLevelPercentage)
			self.updateVolumeLevelLabel(volumeLevelPercentage)
		}
	}
	
	@IBInspectable
	var numOfLines:Int = 10 {
		willSet{
			// TODO: limit the max amount
		}
		didSet {
			self.redrawLines()
		}
	}


	
	//--------------------------------------------------------------------------------------------------------------
	// MARK: - Init
	//--------------------------------------------------------------------------------------------------------------

	override init(frame: CGRect) {
		super.init(frame: frame)
		
		// Add TapGestureRecognizer
		let tapGesture = UITapGestureRecognizer(target: self, action: NSSelectorFromString("hideKeyboard"))
		self.addGestureRecognizer(tapGesture)
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
	
	convenience init(frame:CGRect, volume:Int) {
		self.init(frame:frame, volume: volume, numberOfLines: 10, lineColor:.blueColor())
	}
	
	convenience init(frame:CGRect, volume:Int, numberOfLines:Int) {
		self.init(frame:frame, volume: volume, numberOfLines: numberOfLines, lineColor:.blueColor())
	}
	
	convenience init(frame:CGRect, volume:Int, numberOfLines:Int, lineColor:UIColor) {
		self.init(frame: frame)
		
		self.volumeLevelPercentage = volume
		self.numOfLines = numberOfLines
		self.lineColor = lineColor
	}
	
	
	//--------------------------------------------------------------------------------------------------------------
	// MARK: - UI Updating
	//--------------------------------------------------------------------------------------------------------------
	
	private func reBuildUI(frame:CGRect) {
		var previousElementRightEdge:CGFloat	= 0.0
		//		var previousElementLeftEdge:CGFloat		= 0.0
		//		var previousElementTopEdge:CGFloat		= 0.0
		var previousElementBottomEdge:CGFloat	= 0.0
		//		var previousElementPositionX:CGFloat	= 0.0
		var previousElementPositionY:CGFloat	= 0.0
		var previousElementHeight:CGFloat		= 0.0
		//		var previousElementWidth:CGFloat		= 0.0
		
		
		// Volume control frame
		volumeLevelContainerH						= frame.height / 2 - verticalSpacer*4
		volumeLevelContainerW						= frame.width
		let volumeLevelContainerPositionX:CGFloat	= 0.0
		let volumeLevelContainerPositionY:CGFloat	= frame.height/2 - volumeLevelContainerH/2
		
		// UI elements positioning data
		let inputElementsWidth:CGFloat		= frame.size.width/2
		let inputFieldsPositionX:CGFloat	= 0.0
		
		
		// Volume input field config
		//
		// There should be a font, but its an optional
		if volumeInputField.font == nil {
			volumeInputField.font = .systemFontOfSize(16.0)
		}
		let localizedVolumeInputPlaceholderText:NSString = NSLocalizedString("Volume",
		                                                                     comment: "volume input placeholder text")
		// Get field height for font
		var inputFieldFont			= volumeInputField.font
		var inputFieldHeightForFont = localizedVolumeInputPlaceholderText
			.sizeWithAttributes([NSFontAttributeName : inputFieldFont!]).height
		// Frame setup
		volumeInputField.frame = CGRectMake(inputFieldsPositionX, verticalSpacer,
		                                    inputElementsWidth, inputFieldHeightForFont)
		volumeInputField.borderStyle	= .RoundedRect
		volumeInputField.placeholder	= localizedVolumeInputPlaceholderText as String
		volumeInputField.tag			= TPRVolumeControlInputField.volume.rawValue
		volumeInputField.delegate		= self
		
		
		// Volume input button
		//
		previousElementRightEdge	= inputFieldsPositionX+inputElementsWidth
		previousElementPositionY	= volumeInputField.frame.origin.y
		previousElementHeight		= volumeInputField.bounds.height
		let localizedVolumeButtonTitle	= NSLocalizedString("Set volume", comment: "volume button title").uppercaseString
		volumeInputButton.frame = CGRectMake(previousElementRightEdge+horiziontalSpacer, previousElementPositionY,
		                                     inputElementsWidth-horiziontalSpacer, previousElementHeight)
		volumeInputButton.setTitle(localizedVolumeButtonTitle, forState: .Normal)
		volumeInputButton.addTarget(self, action: #selector(self.volumeButtonPressedAction), forControlEvents: .TouchUpInside)
		volumeInputButton.backgroundColor = .lightGrayColor()
		volumeInputButton.setTitleColor(.blackColor(), forState: .Normal)
		volumeInputButton.setTitleColor(.whiteColor(), forState: .Highlighted)
		volumeInputButton.titleLabel?.font = UIFont.systemFontOfSize(10.0)
		
		
		// Bottom point of Volume input field element
		let prevElementBottomPoint = volumeInputField.frame.origin.y + volumeInputField.bounds.height
		
		
		// Lines input field config
		//
		// There should be a font, but its an optional
		if linesInputField.font == nil {
			linesInputField.font = UIFont.systemFontOfSize(16.0)
		}
		let localizedLinesInputPlaceholderText = NSLocalizedString("Lines",
		                                                           comment: "lines input placeholder text")
		// Get field height for font
		inputFieldFont			= linesInputField.font
		inputFieldHeightForFont = localizedVolumeInputPlaceholderText
			.sizeWithAttributes([NSFontAttributeName : inputFieldFont!]).height
		// Frame setup
		linesInputField.frame = CGRectMake(inputFieldsPositionX, prevElementBottomPoint+verticalSpacer,
		                                   inputElementsWidth, inputFieldHeightForFont)
		linesInputField.borderStyle = .RoundedRect
		linesInputField.placeholder = localizedLinesInputPlaceholderText
		linesInputField.tag			= TPRVolumeControlInputField.lines.rawValue
		linesInputField.delegate	= self
		
		// Lines input button
		//
		let localizedLineButtonTitle = NSLocalizedString("Set lines", comment: "line button title").uppercaseString
		linesInputButton.frame = CGRectMake(previousElementRightEdge+horiziontalSpacer, prevElementBottomPoint+verticalSpacer,
		                                    inputElementsWidth-horiziontalSpacer, previousElementHeight)
		linesInputButton.setTitle(localizedLineButtonTitle, forState: .Normal)
		linesInputButton.addTarget(self, action: #selector(self.lineButtonPressedAction), forControlEvents: .TouchUpInside)
		linesInputButton.backgroundColor = .lightGrayColor()
		linesInputButton.setTitleColor(.blackColor(), forState: .Normal)
		linesInputButton.setTitleColor(.whiteColor(), forState: .Highlighted)
		linesInputButton.titleLabel?.font = UIFont.systemFontOfSize(10.0)
		
		
		// Setup VolumeLevelsContainerView
		//
		volumeLevelContainerView.frame	= CGRectMake(volumeLevelContainerPositionX, volumeLevelContainerPositionY+verticalSpacer*4,
		                              	             volumeLevelContainerW, volumeLevelContainerH)
		
		// Volume level label
		//
		previousElementBottomEdge		= volumeLevelContainerView.frame.origin.y + volumeLevelContainerView.frame.height
		let localizedVolumeLabelText	= NSLocalizedString("Volume set at:", comment: "volume label text")
		// using volumeLevelMAX to set MAX label width
		volumeLevelLabel.text			= String(format: "%@ %d%%", localizedVolumeLabelText, volumeLevelMAX.integerValue)
		// Get label height for font
		volumeLevelLabel.font			= UIFont.systemFontOfSize(12.0)
		let volumeLabelFont				= volumeLevelLabel.font
		let volumeLabelSizeForFont		= volumeLevelLabel.text!
			.sizeWithAttributes([NSFontAttributeName : volumeLabelFont!])
		let volumeLabelPositionX		= self.volumeLevelContainerView.frame.width/2 - volumeLabelSizeForFont.width/2
		volumeLevelLabel.frame			= CGRectMake(volumeLabelPositionX, previousElementBottomEdge,
		                      			             volumeLabelSizeForFont.width, volumeLabelSizeForFont.height)
		
		// Update label value
		self.updateVolumeLevelLabel(volumeLevelPercentage)
		
		// Update lines
		self.redrawLines()
	}
		
	private func buildUI(frame:CGRect) {
		self.reBuildUI(frame)
		
		if self.subviews.count == 0{
			self.addSubview(volumeInputField)
			self.addSubview(volumeInputButton)
			self.addSubview(linesInputField)
			self.addSubview(linesInputButton)
			self.addVolumeLineViews()
			self.addSubview(volumeLevelContainerView)
			self.addSubview(volumeLevelLabel)
		}
	}
	
	override func layoutSubviews() {
		self.reBuildUI(self.frame)
	}
	
	// Update volume lines based on volume
	private func updateVolumeLevelLines(volume: NSNumber) {
		
		let volumeLevelAsLine = self.volumeLevelAsLine(volume)
		for (volumeLineIndex, volumeLine) in volumeLevelContainerView.subviews.enumerate() {
			
			guard volumeLine.isKindOfClass(TPRVolumeLine) else {continue}
			
			// Update state
			if let volumeLine = volumeLine as? TPRVolumeLine {
				volumeLine.enabled =  volumeLineIndex < volumeLevelAsLine
			}
		}
	}
	
	// Update volume label
	private func updateVolumeLevelLabel(volume:NSNumber) {
		let localizedVolumeLabelText = NSLocalizedString("Volume set at:", comment: "volume label text")
		volumeLevelLabel.text = String(format: "%@ %d%%", localizedVolumeLabelText, volume.integerValue)
	}
	
	
	//--------------------------------------------------------------------------------------------------------------
	// MARK: - UIButton action
	//--------------------------------------------------------------------------------------------------------------
	
	dynamic private func volumeButtonPressedAction() {
		self.processTextFieldInput(self.volumeInputField)
		self.hideKeyboard()
	}
	
	dynamic private func lineButtonPressedAction() {
		self.processTextFieldInput(self.linesInputField)
		self.hideKeyboard()
	}
	
	
	//--------------------------------------------------------------------------------------------------------------
	// MARK: - Touch handling
	//--------------------------------------------------------------------------------------------------------------
	
	override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
		self.processTouchInput(touches)
	}
	
	override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
		self.processTouchInput(touches)
	}
	
	//--------------------------------------------------------------------------------------------------------------

	private func processTouchInput(touches: Set<UITouch>) {
		
		// When tapping away from input field, we dont reset the sound/UI, only hide keyboard
		guard !volumeInputField.isFirstResponder() else {
			self.hideKeyboard()
			return
		}
		guard !linesInputField.isFirstResponder() else {
			self.hideKeyboard()
			return
		}
		
		// Otherwise process the touch
		if let touch = touches.first {
			
			// Touches outside of lines are dismissed
			guard CGRectContainsPoint(volumeLevelContainerView.frame, touch.locationInView(self)) else {return}

			// Touch location inside volume level lines
			let touchPointInVolumeContainerView = touch.locationInView(volumeLevelContainerView)
			
			// Update volume level
			volumeLevelPercentage = self.touchAsVolumeLevel(touchPointInVolumeContainerView)
			
			// Change system audio output
			self.changeVolumeLevel(volumeLevelPercentage)
		}
	}
	
	dynamic private func hideKeyboard() {
		if volumeInputField.isFirstResponder() {
			volumeInputField.resignFirstResponder()
		} else if linesInputField.isFirstResponder() {
			linesInputField.resignFirstResponder()
		}
	}
	
	
	//--------------------------------------------------------------------------------------------------------------
	// MARK: - UITextField delegate
	//--------------------------------------------------------------------------------------------------------------

	func textFieldDidEndEditing(textField: UITextField) {
		self.processTextFieldInput(textField)
	}
	
	func textFieldShouldReturn(textField: UITextField) -> Bool {
		self.processTextFieldInput(textField)
		return true
	}
	
	
	//--------------------------------------------------------------------------------------------------------------
	// MARK: - Processing data
	//--------------------------------------------------------------------------------------------------------------

	// Logic to respond to given user input
	func processTextFieldInput(textField: UITextField) {
		// If Valid input
		if let numberFromString = NSNumberFormatter().numberFromString(textField.text!) {
			
			// Volume input capping
			if textField.tag == TPRVolumeControlInputField.volume.rawValue {
				if numberFromString.compare(volumeLevelMAX) == .OrderedDescending {
					volumeLevelPercentage = 100 // limit anything above MAX to 100
				}else if numberFromString.compare(volumeLevelMIN) == .OrderedAscending {
					volumeLevelPercentage = 0 // limit anything below MIN to 0
				}else {
					volumeLevelPercentage = numberFromString.integerValue // input ok
				}
			}
			// Lines input capping
			if textField.tag == TPRVolumeControlInputField.lines.rawValue{
				
				if numberFromString.compare(linesAmountMIN) == .OrderedAscending {
					numOfLines = 0 // limit anything below MIN to 0
				}else {
					numOfLines = numberFromString.integerValue // input ok
				}
			}
			
			// Update UI
			self.updateVolumeLevelLines(volumeLevelPercentage)
			
			// Change system audio output
			self.changeVolumeLevel(volumeLevelPercentage)
		}
	}
	
	// Set global system volume level
	private func changeVolumeLevel(volume: NSNumber) {
		soundManager.setMediaPlayerVolume(volume)
	}
	

	//--------------------------------------------------------------------------------------------------------------
	// MARK: - Helpers
	//--------------------------------------------------------------------------------------------------------------

	// Removes volume lines from container and adds new ones
	private func redrawLines() {
		volumeLevelContainerView.subviews.forEach{$0.removeFromSuperview()}
		self.addVolumeLineViews()
	}
 
	// Creates and adds the UIView objects representing the line to a container view
	private func addVolumeLineViews() {
		// Lines that make up a volume indicator (count includes spacers)
		let levels:Int = (numOfLines*2)-1
		
		// Check parameters
		guard levels > 0 else {return}
		
		// Volume data
		let volumeLevelAsLines:Float = Float(levels) - (Float(levels)/100 * Float(volumeLevelPercentage))
		let volumeLineHeight = CGFloat(volumeLevelContainerH)/CGFloat(levels)
		let volumeLineWidth = volumeLevelContainerW
		var volumeLineFrame = CGRectZero
		
		// Generate volume lines
		for lineNumber in 1...levels+1 {
			
			// Odd lines are volume lines, others are spacers
			if lineNumber % 2 == 0 {
				
				// Calculate line frame with correct position
				
				let lineOffset = levels==1 ? 1 : lineNumber
				let volumeLinePositionY = volumeLevelContainerH-(volumeLineHeight*CGFloat(lineOffset))
				volumeLineFrame = CGRectMake(0, volumeLinePositionY, volumeLineWidth, volumeLineHeight)
				
				// Volume line view object
				let volumeLine = TPRVolumeLine.init(frame: volumeLineFrame, color: lineColor)
				
				// If our line indicates current volume level
				if lineNumber <= Int(floor(volumeLevelAsLines)) {
					volumeLine.enabled = true
				} else {
					volumeLine.enabled = false
				}
				
				// Add creataed line
				volumeLevelContainerView.addSubview(volumeLine)
			}
		}
	}
	
	// Convert volume level to number of lines in view (doesnt count spacers !!)
	private func volumeLevelAsLine(volume: NSNumber) -> Int {
		let allLines = Float(numOfLines)
		let volumeLevelAsLine = Int(floor(allLines/100 * volume.floatValue))
		
		return volumeLevelAsLine
	}
	
	// Convert number of lines to volume level
	private func touchAsVolumeLevel(touchPosition:CGPoint) -> Int {
		var volumeLevel = 100 - (touchPosition.y/volumeLevelContainerH * 100)
		
		if volumeLevel > 100 {volumeLevel = 100}
		if volumeLevel < 0 {volumeLevel = 0}
		
		return Int(volumeLevel)
	}
}