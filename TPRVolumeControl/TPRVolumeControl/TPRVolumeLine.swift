//
//  TPRVolumeLine.swift
//  TPRVolumeControl
//
//  Created by SnowMan on 2016-08-30.
//  Copyright © 2016 test. All rights reserved.
//

import Foundation
import UIKit


class TPRVolumeLine: UIView {
	
	internal var defaultColor = UIColor.blueColor()
	
	var enabled = false {
		didSet{
			if enabled {
				self.tintColor = defaultColor
			} else {
				self.tintColor = UIColor.lightGrayColor()
			}
			self.setNeedsDisplay()
		}
	}
	
	//--------------------------------------------------------------------------------------------------------------
	// MARK: - Init
	//--------------------------------------------------------------------------------------------------------------

	override init(frame: CGRect) {
		super.init(frame: frame)
	}
	
	convenience init(frame: CGRect, color: UIColor){
		self.init(frame: frame)
		self.defaultColor = color
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	//--------------------------------------------------------------------------------------------------------------
	// MARK: -
	//--------------------------------------------------------------------------------------------------------------
	
	override func drawRect(rect: CGRect) {
		self.tintColor.setFill()
		UIRectFill(rect)
	}
	
}